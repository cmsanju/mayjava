package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCalculator {
	
	Calculator obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("before class");
	}
	@AfterClass
	public static void afterClass()
	{
		System.out.println("after class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test");
		obj = new Calculator();
	}
	@After
	public void setDown()
	{
		System.out.println("after test");
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add()");
		assertEquals(30, obj.add(10, 20));
	}
	@Test
	public void testMul()
	{
		System.out.println("test mul");
		assertEquals(50, obj.mul(25, 2));
	}
	@Test
	public void testSub()
	{
		System.out.println("test sub");
		assertEquals(30, obj.sub(50, 20));
	}
	@Test
	public void testGreet()
	{
		System.out.println("test greet");
		assertEquals("hi hello", obj.greet("hi hello"));
	}
}
